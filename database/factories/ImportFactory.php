<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\Import;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class ImportFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Import::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::factory()->create()->id,
            'file_path' => config('imports.temporary_folder') . '/' . Str::slug($this->faker->sentence(rand(1, 5))) . '.csv',
            'file_name' => Str::slug($this->faker->sentence(rand(1, 5))) . '.csv',
            'total_rows' => $this->faker->randomNumber(),
        ];
    }
}
