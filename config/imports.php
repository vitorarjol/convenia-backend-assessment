<?php

return [

    /*
    |--------------------------------------------------------------------------
    | CHUNK SIZE
    |--------------------------------------------------------------------------
    |
    | When performing the import operations, the uploaded file will be chunked
    | to ensure that our php process is able to perform the inserts without
    | extrapolating the memory limits.
    |
    */

    'chunk_size' => env('IMPORTS_CHUNK_SIZE', 10),

    /*
    |--------------------------------------------------------------------------
    | TEMPORARY FOLDER
    |--------------------------------------------------------------------------
    |
    | We are going to store the csv file temporarily so we don't need to rely
    | in the temporary path that PHP gives us when uploading a file.
    |
    */

    'temporary_folder' => env('IMPORTS_TEMPORARY_FOLDER', 'temporary_csv_processing'),

];
