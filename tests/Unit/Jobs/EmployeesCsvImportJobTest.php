<?php

namespace Tests\Unit\Jobs;

use Tests\TestCase;
use App\Models\User;
use App\Models\Import;
use League\Csv\Reader;
use League\Csv\Statement;
use Laravel\Passport\Passport;
use App\Utilities\ChunkIterator;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Storage;
use App\Jobs\Employees\EmployeesCsvImportJob;
use Illuminate\Foundation\Testing\RefreshDatabase;
use League\Csv\TabularDataReader;
use Tests\Helpers\TestHelper;

class EmployeesCsvImportJobTest extends TestCase
{
    use RefreshDatabase;

    protected User $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = Passport::actingAs(
            User::factory()->create()
        );
    }

    public function test_the_jobs_are_dispatched_after_a_successful_processing_of_the_csv_chunks()
    {
        Queue::fake();

        $filePath = TestHelper::createAndStoreACsvFile();

        $import = Import::factory()->create([
            'user_id' => auth()->user()->id,
            'file_path' => $filePath,
        ]);

        $dataReader = $this->getRecordsFromCsv(path: $filePath);

        $chunks = (new ChunkIterator($dataReader->getRecords(), config('imports.chunk_size')))->get();
        $jobs = collect($chunks)
            ->each(fn ($chunk, $index) =>  EmployeesCsvImportJob::dispatch(
                userId: $this->user->id,
                importId: $import->id,
                chunk: $chunk,
                chunkSize: config('imports.chunk_size'),
                currentChunk: $index,
            ))
            ->toArray();

        Queue::assertPushed(EmployeesCsvImportJob::class, count($jobs));
    }

    public function test_the_employee_is_stored_in_the_database_after_the_job_processing()
    {
        $import = Import::factory()->create([
            'user_id' => auth()->user()->id,
        ]);

        $employeeData = [
            'name' => 'Bob Wilson',
            'email' => 'bob@paopaocafe.com',
            'document' => '13001647000',
            'city' => 'Salvador',
            'state' => 'BA',
            'start_date' => '2020-01-15',
        ];

        EmployeesCsvImportJob::dispatch(
            userId: $this->user->id,
            importId: $import->id,
            chunk: [$employeeData],
            chunkSize: config('imports.chunk_size'),
            currentChunk: 1,
        );

        $this->assertDatabaseHas('employees', $employeeData);
        $this->assertDatabaseCount('employees', 1);
    }


    public function test_the_employee_is_updated_in_the_database_after_the_job_processing()
    {
        $import = Import::factory()->create([
            'user_id' => auth()->user()->id,
        ]);

        $employeeData = [
            'name' => 'Marco Rodrigues',
            'email' => 'marco@kyokugen.org',
            'document' => '71306511054',
            'city' => 'Osasco',
            'state' => 'SC',
            'start_date' => '2021-01-10',
        ];

        EmployeesCsvImportJob::dispatch(
            userId: $this->user->id,
            importId: $import->id,
            chunk: [$employeeData],
            chunkSize: config('imports.chunk_size'),
            currentChunk: 1,
        );

        $this->assertDatabaseHas('employees', $employeeData);

        $employeeData['state'] = 'SP';

        EmployeesCsvImportJob::dispatch(
            userId: $this->user->id,
            importId: $import->id,
            chunk: [$employeeData],
            chunkSize: config('imports.chunk_size'),
            currentChunk: 1,
        );

        $this->assertDatabaseHas('employees', $employeeData);
        $this->assertDatabaseCount('employees', 1);
    }

    public function test_the_employee_is_not_stored_in_the_database_after_the_job_processing()
    {
        $import = Import::factory()->create([
            'user_id' => auth()->user()->id,
        ]);

        $employeeData = [
            'name' => 'Jimmy Blanka',
            'email' => 'blanka@enel.com.br',
            'document' => '33010323034',
            'city' => 'Manaus',
            'state' => 'AM',
            'start_date' => '2021-02-29',
        ];

        EmployeesCsvImportJob::dispatch(
            userId: $this->user->id,
            importId: $import->id,
            chunk: [$employeeData],
            chunkSize: config('imports.chunk_size'),
            currentChunk: 1,
        );

        $this->assertDatabaseMissing('employees', $employeeData);
        $this->assertDatabaseCount('employees', 0);
    }

    private function getRecordsFromCsv(string $path): TabularDataReader
    {
        $stream = fopen(Storage::path($path), 'r');
        $csv = Reader::createFromStream($stream);
        $csv->setHeaderOffset(0);

        return Statement::create()->process($csv);
    }
}
