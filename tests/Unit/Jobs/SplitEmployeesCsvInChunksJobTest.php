<?php

namespace Tests\Unit\Jobs;

use Tests\TestCase;
use App\Models\User;
use App\Models\Import;
use Tests\Helpers\TestHelper;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Queue;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Jobs\Employees\SplitEmployeesCsvInChunksJob;

class SplitEmployeesCsvInChunksJobTest extends TestCase
{
    use RefreshDatabase;

    protected User $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = Passport::actingAs(
            User::factory()->create()
        );
    }

    public function test_the_job_batch_is_dispatched_after_the_upload_of_a_valid_csv_file()
    {
        Queue::fake();

        $filePath = TestHelper::createAndStoreACsvFile();

        $import = Import::factory()->create([
            'user_id' => auth()->user()->id,
            'file_path' => $filePath,
        ]);

        SplitEmployeesCsvInChunksJob::dispatch(
            file: $filePath,
            importId: $import->id,
            userId: auth()->user()->id,
            chunkSize: config('imports.chunk_size')
        );

        Queue::assertPushed(SplitEmployeesCsvInChunksJob::class);
    }


    public function test_the_csv_is_spplited_in_chunks_and_the_jobs_are_added_to_the_batch()
    {
        Queue::fake();

        $filePath = TestHelper::createAndStoreACsvFile();

        $import = Import::factory()->create([
            'user_id' => auth()->user()->id,
            'file_path' => $filePath,
        ]);

        [$job, $batch] = (new SplitEmployeesCsvInChunksJob(
            file: $filePath,
            importId: $import->id,
            userId: auth()->user()->id,
            chunkSize: config('imports.chunk_size')
        ))->withFakeBatch();

        $job->handle();

        $this->assertNotEmpty($batch->added);


        $newlyAddedJobs = collect($batch->added)->first();
        $numberOfExpectedChunks = (int) ceil(count(TestHelper::$csvRows) / config('imports.chunk_size'));

        $this->assertTrue(count($newlyAddedJobs ?? []) === $numberOfExpectedChunks);
    }
}
