<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RegistrationController extends TestCase
{
    use RefreshDatabase;

    public function test_new_users_can_register_on_the_application()
    {
        $this
            ->post(route('auth.register'), [
                'name' => 'Convenia User',
                'email' => 'test@convenia.com.br',
                'password' => 'password',
                'password_confirmation' => 'password',
            ])
            ->assertCreated()
            ->assertJson([
                'name' => 'Convenia User',
                'email' => 'test@convenia.com.br',
            ]);

        $this->assertDatabaseHas('users', [
            'email' => 'test@convenia.com.br',
        ]);
    }
}
