<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Tests\Helpers\TestHelper;
use Laravel\Passport\Passport;
use Illuminate\Bus\PendingBatch;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Notification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Notifications\Employees\EmployeesImportFailed;
use App\Notifications\Employees\EmployeesImportFinished;

class ImportEmployeesFromCsvControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_unauthenticated_user_cannot_upload_a_employees_list()
    {
        $this
            ->postJson(route('employees.import.csv'), [
                'employees_list' => []
            ])->assertUnauthorized();
    }

    public function test_the_job_batch_is_dispatched_after_the_upload_of_a_valid_csv_file()
    {
        Bus::fake();

        $this->user = Passport::actingAs(User::factory()->create());

        $file = TestHelper::createAValidCsvFile();

        $this
            ->postJson(route('employees.import.csv'), [
                'employees_list' => $file
            ])
            ->assertOk();

        Bus::assertBatched(
            fn (PendingBatch $batch) =>
            $batch->name == 'import-employees' &&
                $batch->jobs->count() === 1
        );
    }

    public function test_a_notification_is_sent_after_the_processing_of_a_batch_of_jobs()
    {
        Bus::fake();
        Notification::fake();

        $this->user = Passport::actingAs(User::factory()->create());

        $file = TestHelper::createAValidCsvFile();

        $this
            ->postJson(route('employees.import.csv'), [
                'employees_list' => $file
            ])
            ->assertOk();

        Bus::assertBatched(function (PendingBatch $batch) {
            /* @var \Illuminate\Queue\SerializableClosure $catchCallback */
            [$catchCallback] = $batch->catchCallbacks();
            $catchCallback->getClosure()->call($this);
            Notification::assertSentTo(
                [$this->user],
                EmployeesImportFailed::class
            );

            /* @var \Illuminate\Queue\SerializableClosure $finnalyCallback */
            [$finnalyCallback] = $batch->finallyCallbacks();
            $finnalyCallback->getClosure()->call($this);
            Notification::assertSentTo(
                [$this->user],
                EmployeesImportFinished::class
            );

            return $batch->name == 'import-employees' && $batch->jobs->count() === 1;
        });
    }
}
