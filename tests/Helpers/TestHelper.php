<?php

namespace Tests\Helpers;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class TestHelper
{
    public static string $csvHeaders = 'name,email,document,city,state,start_date';

    public static array $csvRows = [
        'Bob Wilson,bob@paopaocafe.com,13001647000,Salvador,BA,2020-01-15',
        'Laura Matsuda,lm@matsuda.com.br,60095284028,Niterói,RJ,2019-06-08',
        'Marco Rodrigues ,marco@kyokugen.org,71306511054,Osasco,SC,2021-01-10',
        'Christie Monteiro,monteiro@namco.com,28586454001,Recife,PE,2015-11-01',
    ];

    public static function createAndStoreACsvFile(): string
    {
        $file = self::createAValidCsvFile();

        return Storage::put(config('imports.temporary_folder'), $file);
    }

    public static function createAValidCsvFile(): UploadedFile
    {
        Storage::fake();

        return UploadedFile::fake()->createWithContent(
            'employees_list.csv',
            implode("\n", [self::$csvHeaders, ...self::$csvRows])
        );
    }

    public static function createAnInvalidCsv(): UploadedFile
    {
        Storage::fake();

        $rows = [
            'Jimmy Blanka,blanka@enel.com.br,33010323034,Manaus,AM,2021-02-29'
        ];

        return UploadedFile::fake()->createWithContent(
            'employees_list.csv',
            implode("\n", [self::$csvHeaders, ...$rows])
        );
    }
}
