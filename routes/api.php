<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(
    [
        'prefix' => '/auth',
        'as' => 'auth.',
    ],
    function () {
        Route::post('/register', App\Http\Controllers\Auth\RegistrationController::class)
            ->middleware('guest')
            ->name('register');
    }
);

Route::post('/user', [Laravel\Passport\Http\Controllers\AccessTokenController::class, 'issueToken'])
    ->name('token')
    ->middleware(['guest', 'throttle']);

Route::group(['middleware' => 'auth:api'], function () {

    Route::get('/employees', [\App\Http\Controllers\EmployeeController::class, 'get'])
        ->name('employees.get');

    Route::get('/employees/{employee}', [\App\Http\Controllers\EmployeeController::class, 'show'])
        ->name('employees.show');

    Route::delete('/employees/{employee}', [\App\Http\Controllers\EmployeeController::class, 'destroy'])
        ->name('employees.destroy');

    Route::post('/employees/import-csv', \App\Http\Controllers\ImportEmployeesFromCsvController::class)
        ->name('employees.import.csv');
});
