FROM composer:latest

# # For PHPUnit to work fully
# RUN docker-php-ext-install pcntl

RUN docker-php-ext-install pcntl \
    && docker-php-ext-configure pcntl --enable-pcntl