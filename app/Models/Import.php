<?php

namespace App\Models;

use App\Models\ImportError;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Import
 *
 * @property int $errors_count
 */
class Import extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'file_path',
        'file_name',
        'total_rows',
        'processed_rows',
        'successful_rows',
        'invalid_rows',
        'completed_at'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    public $casts = [
        'completed_at' => 'datetime'
    ];


    /**
     * The errors of that import that should be cast.
     *
     * @return HasMany<ImportError>
     */
    public function errors()
    {
        return $this->hasMany(ImportError::class, 'import_id');
    }
}
