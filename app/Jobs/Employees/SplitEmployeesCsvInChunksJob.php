<?php

namespace App\Jobs\Employees;

use App\Models\Import;
use League\Csv\Reader;
use League\Csv\Statement;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use App\Utilities\ChunkIterator;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SplitEmployeesCsvInChunksJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Batchable;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(public string $file, public int $chunkSize, public int $importId, public int $userId)
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        if ($this->batch()->cancelled()) {
            return;
        }

        // Open the csv
        $stream = fopen(Storage::path($this->file), 'r');
        $csv = Reader::createFromStream($stream);
        $csv->setHeaderOffset(0);

        // Load the records
        $records = Statement::create()->process($csv);

        Import::query()
            ->where('id', $this->importId)
            ->update([
                'total_rows' => count($records),
            ]);

        // Build the job chunks
        $chunks = (new ChunkIterator($records->getRecords(), $this->chunkSize))->get();
        $jobs = collect($chunks)
            ->map(fn ($chunk, $index) =>  new EmployeesCsvImportJob(
                userId: $this->userId,
                importId: $this->importId,
                chunk: $chunk,
                chunkSize: $this->chunkSize,
                currentChunk: $index,
            ))
            ->toArray();

        $this->batch()->add($jobs);
    }
}
