<?php

namespace App\Jobs\Employees;

use App\Models\Import;
use App\Models\Employee;
use App\Models\ImportError;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\DB;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class EmployeesCsvImportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Batchable;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(public int $userId, public int $importId, public array $chunk, public int $chunkSize, public int $currentChunk)
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        $validRows = collect($this->chunk)
            ->filter(fn ($row, $rowIndex) => $this->ensureRowIsValid($row, $rowIndex))
            ->map(function ($row) {
                $row['document'] = preg_replace('/[^0-9]/', '', $row['document']);

                return array_merge(['user_id' => $this->userId], $row);
            })
            ->toArray();

        $employeeIdentifiers = ['document'];
        $fieldsToBeUpdated = [
            'name',
            'email',
            'city',
            'state',
            'start_date',
        ];

        Employee::upsert($validRows, $employeeIdentifiers, $fieldsToBeUpdated);

        Import::query()
            ->where('id', $this->importId)
            ->update([
                'processed_rows' => DB::raw('processed_rows + ' . count($this->chunk)),
                'successful_rows' => DB::raw('successful_rows + ' . count($validRows)),
                'invalid_rows' => DB::raw('invalid_rows + ' . (count($this->chunk) - count($validRows))),
            ]);
    }

    public function ensureRowIsValid(array $row, int $rowIndex): bool
    {
        $validator = Validator::make($row, [
            'name' => 'required|string',
            'email' => 'required|email',
            'document' => 'required|string',
            'city' => 'required|string',
            'state' => 'required|size:2',
            'start_date' => 'required|date',
        ], [
            'start_date.date' => 'NOT ACCEPTABLE: Invalid Date'
        ]);

        if ($validator->fails()) {
            ImportError::create([
                'import_id' => $this->importId,
                'line' => $this->currentChunk * $this->chunkSize + ($rowIndex + 2),
                'errors' => $validator->errors()->all()
            ]);

            return false;
        }

        return true;
    }
}
