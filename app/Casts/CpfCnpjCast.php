<?php

namespace App\Casts;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

class CpfCnpjCast implements CastsAttributes
{
    public function get($model, $key, $value, $attributes)
    {
        if (!$value) {
            return null;
        }

        $mask = '###.###.###-##';

        if (strlen($value) <= 11) {
            $mask = '###.###.###-##';
            for ($i = strlen($value); $i < 11; $i++) {
                $value = '0' . $value;
            }
        }

        if (strlen($value) > 11) {
            $mask = '##.###.###/####-##';
            for ($i = strlen($value); $i < 14; $i++) {
                $value = '0' . $value;
            }
        }

        $formattedValue = '';
        $k = 0;

        for ($i = 0; $i <= strlen($mask) - 1; $i++) {
            if ($mask[$i] == '#') {
                if (isset($value[$k])) {
                    $formattedValue .= $value[$k++];
                }
            } else {
                $formattedValue .= $mask[$i];
            }
        }

        return $formattedValue;
    }

    public function set($model, $key, $value, $attributes)
    {
        return preg_replace('/[^0-9]/', '', $value);
    }
}
