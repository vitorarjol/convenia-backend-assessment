<?php

namespace App\Utilities;

use Generator;
use Iterator;

class ChunkIterator
{
    public function __construct(protected Iterator $iterator, protected int $numberOfItemsPerChunk)
    {
    }

    public function get(): Generator
    {
        $chunk = [];

        while ($this->iterator->valid()) {
            $chunk[] = $this->iterator->current();
            $this->iterator->next();

            if (count($chunk) === $this->numberOfItemsPerChunk) {
                yield $chunk;
                $chunk = [];
            }
        }

        if (count($chunk)) {
            yield $chunk;
        }
    }
}
