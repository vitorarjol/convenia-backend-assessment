<?php

namespace App\Http\Controllers;

use App\Models\Import;
use App\Http\Requests\Employees\ImportEmployeesFromCsvRequest;
use App\Jobs\Employees\SplitEmployeesCsvInChunksJob;
use App\Notifications\Employees\EmployeesImportFailed;
use App\Notifications\Employees\EmployeesImportFinished;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Storage;

class ImportEmployeesFromCsvController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \App\Http\Requests\Employees\ImportEmployeesFromCsvRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(ImportEmployeesFromCsvRequest $request): JsonResponse
    {
        $currentUser = auth()->user();
        $file = $request->file('employees_list');

        $temporaryFilePath = config('imports.temporary_folder') . '/' . $file->hashName();

        Storage::put(config('imports.temporary_folder'), $file);

        $importId = Import::insertGetId([
            'user_id' => auth()->user()->id,
            'file_path' => $temporaryFilePath,
            'file_name' => $file->getClientOriginalName(),
        ]);

        Bus::batch([
            new SplitEmployeesCsvInChunksJob(
                file: $temporaryFilePath,
                importId: $importId,
                userId: auth()->user()->id,
                chunkSize: config('imports.chunk_size')
            )
        ])
            ->catch(function () use ($importId, $currentUser) {
                $currentUser->notify(new EmployeesImportFailed(importId: $importId));
            })
            ->finally(function () use ($importId, $temporaryFilePath, $currentUser) {
                Import::query()
                    ->where('id', $importId)
                    ->update([
                        'completed_at' => now(),
                    ]);

                Storage::delete($temporaryFilePath);

                $currentUser->notify(new EmployeesImportFinished(importId: $importId));
            })
            ->name('import-employees')
            ->dispatch();

        return response()->json([
            'message' => 'Your file was uploaded successfully. Our systems are processing the employee data, and you will receive an email with the results shortly.'
        ], JsonResponse::HTTP_OK);
    }
}
