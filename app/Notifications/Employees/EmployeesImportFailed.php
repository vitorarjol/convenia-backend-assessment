<?php

namespace App\Notifications\Employees;

use App\Models\Import;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class EmployeesImportFailed extends Notification
{
    use Queueable;

    public Import $import;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(public int $importId)
    {
        $this->import = Import::findOrFail($importId);
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array<string>
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('There was a issue with your import.')
            ->line("Your csv spreadsheet with {$this->import->total_rows} could not be imported. Please check the integrity of your file and try again.")
            ->line('Thank you for using our application!');
    }
}
