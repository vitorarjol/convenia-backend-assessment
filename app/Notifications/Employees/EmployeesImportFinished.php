<?php

namespace App\Notifications\Employees;

use App\Models\Import;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class EmployeesImportFinished extends Notification
{
    use Queueable;

    public Import $import;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(public int $importId)
    {
        $this->import = Import::withCount('errors')->where('id', $importId)->firstOrFail();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array<string>
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Employees import completed. Check your results')
            ->line('Your employee spreadsheet has been processed. Check your results below:')
            ->line("We processed {$this->import->processed_rows} of {$this->import->total_rows} rows, with {$this->import->successful_rows} successfull updates and {$this->import->invalid_rows} invalid rows.")
            ->lineIf($this->import->errors_count > 0, "The error messages are available in our dashboard. Be sure to check them out")
            ->line('Thank you for using our application!');
    }
}
